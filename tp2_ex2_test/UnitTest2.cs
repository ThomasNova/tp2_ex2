using System;
using Xunit;
using tp2_ex2_console;

namespace tp2_ex2_test
{
    public class UnitTest2
    {
        [Fact]
        public void TestPositionValeur()
        {
            int[] tableauints = {12,20,24,42,16,18};
            Assert.Equal(0, Program1.PositionValeur(tableauints,12));
            Assert.Equal(1, Program1.PositionValeur(tableauints,20));
            Assert.Equal(3, Program1.PositionValeur(tableauints,42));
            Assert.Equal(4, Program1.PositionValeur(tableauints,16));
            
      
        }
    }
}
